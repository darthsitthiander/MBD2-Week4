package com.sanderschnydrig.mbd2_week4;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Sander on 13-3-2015.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.prefs);
    }
}