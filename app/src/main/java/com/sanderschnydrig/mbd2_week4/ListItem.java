package com.sanderschnydrig.mbd2_week4;

import java.io.Serializable;

/**
 * Created by Sander on 12-3-2015.
 */
public class ListItem implements Serializable {
    private String id;
    private String title;
    private String body;

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setBody(String body){
        this.body = body;
    }

    public String getBody(){
        return this.body;
    }

    public String getId(){
        return this.id;
    }
}