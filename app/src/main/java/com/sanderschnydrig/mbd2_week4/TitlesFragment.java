package com.sanderschnydrig.mbd2_week4;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

// Shows the title fragment which is a ListView
// When a ListView item is selected we will put the DetailsFragment in the
// Framelayout if we are in horizontal mode, or we will create a DetailsActivity // and switch to it if in portrait mode
public class TitlesFragment extends Fragment {

    // True or False depending on if we are in horizontal or duel pane mode
    boolean mDuelPane;

    // Currently selected item in the ListView
    int mCurCheckPosition = 0;
    private String[] stringArray ;
    private ArrayAdapter itemArrayAdapter;
    private ListView list;
    private SharedPreferences prefs;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check if the FrameLayout with the id details exists
        View detailsFrame = getActivity().findViewById(R.id.details);

        // Set mDuelPane based on whether you are in the horizontal layout
        // Check if the detailsFrame exists and if it is visible
        mDuelPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        // If the screen is rotated onSaveInstanceState() below will store the // hero most recently selected. Get the value attached to curChoice and // store it in mCurCheckPosition
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mCurCheckPosition = savedInstanceState.getInt("mCurCheckPosition", 0);
        } else {
            prefs = getActivity().getSharedPreferences("com.sanderschnydrig.mbd2-week4", Context.MODE_PRIVATE);
            mCurCheckPosition = prefs.getInt("mCurCheckPosition",0);
        }

        // selects list item in landscape mode
        if (mDuelPane) {
            // CHOICE_MODE_SINGLE allows one item in the ListView to be selected at a time
            // CHOICE_MODE_MULTIPLE allows multiple
            // CHOICE_MODE_NONE is the default and the item won't be highlighted in this case'
            list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            // Send the item selected to showDetails so the right hero info is shown
            showDetails(mCurCheckPosition);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // An ArrayAdapter connects the array to our ListView
        // getActivity() returns a Context so we have the resources needed
        // We pass a default list item text view to put the data in and the
        // array

        List<ListItem> items = new ArrayList<ListItem>();

        for ( int i = 0; i < 20; i++ ) {
            ListItem item = new ListItem();
            item.setTitle("Item Header # " +i);
            item.setBody("Some random body text for item # " +i);
            items.add(item);
        }

        itemArrayAdapter = new customListAdapter(getActivity(), items);

        list = new ListView(getActivity());
        View header = (View) getActivity().getLayoutInflater().inflate(R.layout.listview_header_row, null);
        list.addHeaderView(header);

        list.setAdapter(itemArrayAdapter);

        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {

                        showDetails(position);
                    }
                }
        );

        registerForContextMenu(list);


        return list;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Actions");

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.listview_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_Toast:
                Toast.makeText(getActivity(), "Toast action invoked", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    // Called every time the screen orientation changes or Android kills an Activity
    // to conserve resources
    // We save the last item selected in the list here and attach it to the key curChoice
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mCurCheckPosition", mCurCheckPosition);
    }

    // Shows the hero data
    public void showDetails(int index) {

        // The most recently selected hero in the ListView is sent
        mCurCheckPosition = index;

        // Check if we are in horizontal mode and if yes show the ListView and
        // the hero data
        if (mDuelPane) {

            // Make the currently selected item highlighted
            list.setItemChecked(index, true);

            // Create an object that represents the current FrameLayout that we will
            // put the hero data in
            DetailsFragment details = (DetailsFragment)
                    getFragmentManager().findFragmentById(R.id.details);

            // When a DetailsFragment is created by calling newInstance the index for the data
            // it is supposed to show is passed to it. If that index hasn't been assigned we must
            // assign it in the if block
            if (details == null || details.getShownIndex() != index) {

                // Make the details fragment and give it the currently selected hero index
                details = DetailsFragment.newInstance(index);

                // Start Fragment transactions
                FragmentTransaction ft = getFragmentManager().beginTransaction();

                // Replace any other Fragment with our new Details Fragment with the right data
                ft.replace(R.id.details, details);

                // TRANSIT_FRAGMENT_FADE calls for the Fragment to fade away
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }

        } else {
            // Pass along the currently selected index assigned to the keyword index
            ListItem listItem = (ListItem) list.getAdapter().getItem(index);
            Intent i = new Intent(getActivity(), DetailsActivity.class);
            i.putExtra("item", listItem);
            startActivity(i);
        }
    }
}