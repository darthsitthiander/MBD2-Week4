package com.sanderschnydrig.mbd2_week4;

        import android.app.Fragment;
        import android.content.res.Configuration;
        import android.os.Bundle;
        import android.util.TypedValue;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ScrollView;
        import android.widget.TextView;

public class DetailsFragment extends Fragment {

    // LayoutInflator puts the Fragment on the screen
    // ViewGroup is the view you want to attach the Fragment to
    // Bundle stores key value pairs so that data can be saved
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }

        View view =  inflater.inflate(R.layout.listview_item_detail, container, false);

        ListItem lI = (ListItem) getActivity().getIntent().getSerializableExtra("item");

        if (lI == null) {
            lI = new ListItem();
            lI.setBody("Some random body text for item # "+getShownIndex());
            lI.setTitle("Item Header # " + (getShownIndex()-1) );
        }
        TextView tvTitle = (TextView) view.findViewById(R.id.itemTitle);
        tvTitle.setText(lI.getTitle());
        TextView tvBody = (TextView) view.findViewById(R.id.itemBody);
        tvBody.setText(lI.getBody());

        return view;
    }

    // Create a DetailsFragment that contains the hero data for the correct index
    public static DetailsFragment newInstance(int index) {
        DetailsFragment f = new DetailsFragment();

        // Bundles are used to pass data using a key "index" and a value
        Bundle args = new Bundle();
        args.putInt("index", index);

        // Assign key value to the fragment
        f.setArguments(args);

        return f;
    }

    public int getShownIndex() {

        // Returns the index assigned
        int val = -1;
        if(getArguments() != null){
            val = getArguments().getInt("index", 0);
        }
        return val ;
    }
}