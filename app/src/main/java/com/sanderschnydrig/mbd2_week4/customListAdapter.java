package com.sanderschnydrig.mbd2_week4;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sander on 12-3-2015.
 */
public class customListAdapter extends ArrayAdapter<ListItem>{
    private LayoutInflater inflater;

    public customListAdapter(Activity activity, List<ListItem> items) {
        super(activity, R.layout.listview_item_row, items);
        inflater = activity.getWindow().getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if( convertView == null ){
            //We must create a View:
            convertView = inflater.inflate(R.layout.listview_item_row, parent, false);
        }
        TextView header = (TextView) convertView.findViewById(R.id.textView1);
        header.setText(getItem(position).getTitle());
        TextView body = (TextView) convertView.findViewById(R.id.textView2);
        body.setText(getItem(position).getBody());
        //Here we can do changes to the convertView, such as set a text on a TextView
        //or an image on an ImageView.
        return convertView;
    }

}